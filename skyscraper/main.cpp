#include <QApplication>
#include <QVBoxLayout>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow *window = new MainWindow(NULL);

    // resize the window
    window->resize(1250, 1000);

    // show the label
    window->show();

    app.exec();

    // Clean up.
    delete window;

    return 0;
}
