CONFIG += c++11

TEMPLATE = app
TARGET = skyscraper
INCLUDEPATH += . /opt/local/include

QT += core widgets opengl gui

windows: { LIBS += -lOpenGL32 -lGLU32 }
!windows: { LIBS += -lGLU }

SOURCES += main.cpp \
           mainwindow.cpp \
           viewport.cpp

HEADERS += mainwindow.h \
           materials.h \
           viewport.h
