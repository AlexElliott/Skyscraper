#include <QLabel>
#include <QSpacerItem>
#include "mainwindow.h"
#include "viewport.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{ // Constructor
    // Create the window layout.
    windowLayout = new QBoxLayout(QBoxLayout::LeftToRight, this);
    viewportLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    settingsLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    windowLayout->addLayout(viewportLayout, 3);
    windowLayout->addLayout(settingsLayout, 1);

    // Create main widget.
    viewWidget = new viewport(this);
    viewportLayout->addWidget(viewWidget, 15);

    // Create y slider and label.
    QLabel *ySliderLabel = new QLabel(this);
    ySliderLabel->setText("y-axis rotation:");
    viewportLayout->addWidget(ySliderLabel, 1);
    nVerticesySlider = new QSlider(Qt::Horizontal);
    nVerticesySlider->setMinimum(0);
    nVerticesySlider->setMaximum(360);
    nVerticesySlider->setValue(0);
    viewportLayout->addWidget(nVerticesySlider);

    // Create x slider and label.
    QLabel *xSliderLabel = new QLabel(this);
    xSliderLabel->setText("x-axis rotation:");
    viewportLayout->addWidget(xSliderLabel, 1);
    nVerticesxSlider = new QSlider(Qt::Horizontal);
    nVerticesxSlider->setMinimum(0);
    nVerticesxSlider->setMaximum(360);
    nVerticesxSlider->setValue(0);
    viewportLayout->addWidget(nVerticesxSlider);

    // Create z slider and label.
    QLabel *zSliderLabel = new QLabel(this);
    zSliderLabel->setText("z-axis rotation:");
    viewportLayout->addWidget(zSliderLabel, 1);
    nVerticeszSlider = new QSlider(Qt::Horizontal);
    nVerticeszSlider->setMinimum(0);
    nVerticeszSlider->setMaximum(360);
    nVerticeszSlider->setValue(0);
    viewportLayout->addWidget(nVerticeszSlider);

    // Connect the sliders to their update functions.
    connect(this->nVerticesySlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(yupdate(int)));
    connect(this->nVerticesxSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(xupdate(int)));
    connect(this->nVerticeszSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(zupdate(int)));

    // Create building width slider and label.
    QLabel *buildwSliderLabel = new QLabel(this);
    buildwSliderLabel->setText("skyscraper width:");
    settingsLayout->addWidget(buildwSliderLabel);
    buildwSlider = new QSlider(Qt::Horizontal);
    buildwSlider->setMinimum(12);
    buildwSlider->setMaximum(24);
    buildwSlider->setValue(16);
    settingsLayout->addWidget(buildwSlider);

    // Create building height slider and label.
    QLabel *buildhSliderLabel = new QLabel(this);
    buildhSliderLabel->setText("skyscraper height:");
    settingsLayout->addWidget(buildhSliderLabel);
    buildhSlider = new QSlider(Qt::Horizontal);
    buildhSlider->setMinimum(16);
    buildhSlider->setMaximum(48);
    buildhSlider->setValue(32);
    settingsLayout->addWidget(buildhSlider);

    // Create inner pavement width slider and label.
    QLabel *pave1wSliderLabel = new QLabel(this);
    pave1wSliderLabel->setText("inner pavement width:");
    settingsLayout->addWidget(pave1wSliderLabel);
    pave1wSlider = new QSlider(Qt::Horizontal);
    pave1wSlider->setMinimum(8);
    pave1wSlider->setMaximum(20);
    pave1wSlider->setValue(10);
    settingsLayout->addWidget(pave1wSlider);

    // Create road width slider and label.
    QLabel *roadwSliderLabel = new QLabel(this);
    roadwSliderLabel->setText("road width:");
    settingsLayout->addWidget(roadwSliderLabel);
    roadwSlider = new QSlider(Qt::Horizontal);
    roadwSlider->setMinimum(10);
    roadwSlider->setMaximum(20);
    roadwSlider->setValue(15);
    settingsLayout->addWidget(roadwSlider);

    // Create outer pavement width slider and label.
    QLabel *pave2wSliderLabel = new QLabel(this);
    pave2wSliderLabel->setText("outer pavement width:");
    settingsLayout->addWidget(pave2wSliderLabel);
    pave2wSlider = new QSlider(Qt::Horizontal);
    pave2wSlider->setMinimum(0);
    pave2wSlider->setMaximum(15);
    pave2wSlider->setValue(10);
    settingsLayout->addWidget(pave2wSlider);

    // Create door rotation speed slider and label.
    QLabel *doorSliderLabel = new QLabel(this);
    doorSliderLabel->setText("door rotation speed:");
    settingsLayout->addWidget(doorSliderLabel);
    doorSlider = new QSlider(Qt::Horizontal);
    doorSlider->setMinimum(5);
    doorSlider->setMaximum(20);
    doorSlider->setValue(10);
    settingsLayout->addWidget(doorSlider);

    // Create lift speed slider and label.
    QLabel *liftSliderLabel = new QLabel(this);
    liftSliderLabel->setText("lift speed:");
    settingsLayout->addWidget(liftSliderLabel);
    liftSlider = new QSlider(Qt::Horizontal);
    liftSlider->setMinimum(5);
    liftSlider->setMaximum(20);
    liftSlider->setValue(10);
    settingsLayout->addWidget(liftSlider);

    // Create car speed slider and label.
    QLabel *carSliderLabel = new QLabel(this);
    carSliderLabel->setText("car speed:");
    settingsLayout->addWidget(carSliderLabel);
    carSlider = new QSlider(Qt::Horizontal);
    carSlider->setMinimum(5);
    carSlider->setMaximum(20);
    carSlider->setValue(10);
    settingsLayout->addWidget(carSlider);

    // Connect the sliders to their update functions.
    connect(this->buildwSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(buildwupdate(int)));
    connect(this->buildhSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(buildhupdate(int)));
    connect(this->pave1wSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(pave1wupdate(int)));
    connect(this->roadwSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(roadwupdate(int)));
    connect(this->pave2wSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(pave2wupdate(int)));
    connect(this->doorSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(doorspeedupdate(int)));
    connect(this->liftSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(liftspeedupdate(int)));
    connect(this->carSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(carspeedupdate(int)));

    // Create door stop/start button.
    QLabel *doorToggleLabel = new QLabel(this);
    doorToggleLabel->setText("toggle door:");
    settingsLayout->addWidget(doorToggleLabel);
    doorToggle = new QPushButton("Stop/Start");
    settingsLayout->addWidget(doorToggle);

    // Create door reverse button.
    QLabel *doorReverseLabel = new QLabel(this);
    doorReverseLabel->setText("reverse door direction:");
    settingsLayout->addWidget(doorReverseLabel);
    doorReverse = new QPushButton("Reverse");
    settingsLayout->addWidget(doorReverse);

    // Create lift stop/start button.
    QLabel *liftToggleLabel = new QLabel(this);
    liftToggleLabel->setText("toggle lift:");
    settingsLayout->addWidget(liftToggleLabel);
    liftToggle = new QPushButton("Stop/Start");
    settingsLayout->addWidget(liftToggle);

    // Create car stop/start button.
    QLabel *carToggleLabel = new QLabel(this);
    carToggleLabel->setText("toggle car:");
    settingsLayout->addWidget(carToggleLabel);
    carToggle = new QPushButton("Stop/Start");
    settingsLayout->addWidget(carToggle);

    // Create car reverse button.
    QLabel *carReverseLabel = new QLabel(this);
    carReverseLabel->setText("reverse car direction:");
    settingsLayout->addWidget(carReverseLabel);
    carReverse = new QPushButton("Reverse");
    settingsLayout->addWidget(carReverse);

    // Connect the buttons to their update functions.
    connect(this->doorToggle, SIGNAL(clicked()), viewWidget, SLOT(doortoggleupdate()));
    connect(this->doorReverse, SIGNAL(clicked()), viewWidget, SLOT(doorreverseupdate()));
    connect(this->liftToggle, SIGNAL(clicked()), viewWidget, SLOT(lifttoggleupdate()));
    connect(this->carToggle, SIGNAL(clicked()), viewWidget, SLOT(cartoggleupdate()));
    connect(this->carReverse, SIGNAL(clicked()), viewWidget, SLOT(carreverseupdate()));
}

MainWindow::~MainWindow()
{ // Destructor
    delete viewWidget;
    delete windowLayout;
}

void MainWindow::ResetInterface()
    { // ResetInterface()

    // now force refresh

    update();
    } // ResetInterface()
