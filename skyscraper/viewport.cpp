#include <GL/glu.h>
#include <QGLWidget>
#include "viewport.h"
#include "materials.h"

// Constructor
viewport::viewport(QWidget *parent)
    : QGLWidget(parent)
{
    connect(this->doortimer, SIGNAL(timeout()), this, SLOT(doorupdate()));
    connect(this->lifttimer, SIGNAL(timeout()), this, SLOT(liftupdate()));
    connect(this->cartimer, SIGNAL(timeout()), this, SLOT(carupdate()));
    doortimer->start(doorspeed);
    lifttimer->start(liftspeed);
    cartimer->start(carspeed);

    glGenTextures(1, &marcTexture);
    glGenTextures(1, &markusTexture);
    glGenTextures(1, &worldTexture);
}

// called when OpenGL context is set up
void viewport::initializeGL()
{
    // Set the widget background colour.
    glClearColor(0.3, 0.3, 0.3, 0.0);
}


// Called every time the widget is resized.
void viewport::resizeGL(int w, int h)
{
    // Set the viewport to the entire widget.
    glViewport(0, 0, w, h);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // This defines the view frustum,
    glOrtho(-125.0, 125.0, -100.0, 100.0, -100.0, 200.0);
    //glOrtho(-50.0, 50.0, -50.0, 50.0, -75.0, 100.0);  //Debug view.
}

void viewport::cuboid(materialStruct* material, int width, int height, int depth)
{
  // Here are the normals, correctly calculated for the cube faces below
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0., 1., 0.}, {0., -1., 0.} };

  glMaterialfv(GL_FRONT, GL_AMBIENT,    material->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    material->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   material->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   material->shininess);

  // Front face
  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    glVertex3f( width/2,  height/2,  depth/2);
    glVertex3f( width/2,  height/2, -depth/2);
    glVertex3f( width/2, -height/2, -depth/2);
    glVertex3f( width/2, -height/2,  depth/2);
  glEnd();

  // Back face
  glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f(-width/2,  height/2,  depth/2);
    glVertex3f(-width/2,  height/2, -depth/2);
    glVertex3f(-width/2, -height/2, -depth/2);
    glVertex3f(-width/2, -height/2,  depth/2);
  glEnd();

  // Right face
  glNormal3fv(normals[2]);
  glBegin(GL_POLYGON);
    glVertex3f( width/2,  height/2,  depth/2);
    glVertex3f( width/2, -height/2,  depth/2);
    glVertex3f(-width/2, -height/2,  depth/2);
    glVertex3f(-width/2,  height/2,  depth/2);
  glEnd();

  // Left face
  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f( width/2,  height/2, -depth/2);
    glVertex3f( width/2, -height/2, -depth/2);
    glVertex3f(-width/2, -height/2, -depth/2);
    glVertex3f(-width/2,  height/2, -depth/2);
  glEnd();

  // Top face
  glNormal3fv(normals[4]);
  glBegin(GL_POLYGON);
    glVertex3f( width/2,  height/2,  depth/2);
    glVertex3f( width/2,  height/2, -depth/2);
    glVertex3f(-width/2,  height/2, -depth/2);
    glVertex3f(-width/2,  height/2,  depth/2);
  glEnd();

  // Bottom face
  glNormal3fv(normals[5]);
  glBegin(GL_POLYGON);
    glVertex3f( width/2, -height/2,  depth/2);
    glVertex3f( width/2, -height/2, -depth/2);
    glVertex3f(-width/2, -height/2, -depth/2);
    glVertex3f(-width/2, -height/2,  depth/2);
  glEnd();
}

void viewport::skyscraper(int width, int height, int depth)
{
    // Here are the normals, correctly calculated for the cube faces below
    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0., 1., 0.}, {0., -1., 0.} };

    // Top face
    // This is done first so that the material doesn't interfere with the texture.
    glEnable(GL_TEXTURE_2D);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glEnable(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_2D, marcTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, marcSwap.width(), marcSwap.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, marcSwap.bits());

    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( width/2,  height/2,  depth/2);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( width/2,  height/2, -depth/2);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-width/2,  height/2, -depth/2);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-width/2,  height/2,  depth/2);
    glEnd();

    glDisable(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);

    glMaterialfv(GL_FRONT, GL_AMBIENT,    buildingMaterial.ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    buildingMaterial.diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   buildingMaterial.specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   buildingMaterial.shininess);

    // Front face
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2,  height/2,  depth/2);
        glVertex3f( width/2,  height/2, -depth/2);
        glVertex3f( width/2, -height/2, -depth/2);
        glVertex3f( width/2, -height/2,  depth/2);
    glEnd();

    // Back face
    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
        glVertex3f(-width/2,  height/2,  depth/2);
        glVertex3f(-width/2,  height/2, -depth/2);
        glVertex3f(-width/2, -height/2, -depth/2);
        glVertex3f(-width/2, -height/2,  depth/2);
    glEnd();

    // Right face
    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2,  height/2,  depth/2);
        glVertex3f( width/2, -height/2,  depth/2);
        glVertex3f(-width/2, -height/2,  depth/2);
        glVertex3f(-width/2,  height/2,  depth/2);
    glEnd();

    // Left face
    glNormal3fv(normals[3]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2,  height/2, -depth/2);
        glVertex3f( width/2, -height/2, -depth/2);
        glVertex3f(-width/2, -height/2, -depth/2);
        glVertex3f(-width/2,  height/2, -depth/2);
    glEnd();

    // Bottom face
    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2, -height/2,  depth/2);
        glVertex3f( width/2, -height/2, -depth/2);
        glVertex3f(-width/2, -height/2, -depth/2);
        glVertex3f(-width/2, -height/2,  depth/2);
    glEnd();
}

void viewport::tree()
{
    glPushMatrix();
        glRotatef(-90,1,0,0);
        GLUquadric *quadric = gluNewQuadric();

        glMaterialfv(GL_FRONT, GL_AMBIENT,    woodMaterial.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    woodMaterial.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   woodMaterial.specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   woodMaterial.shininess);

        gluCylinder(quadric, 3, 3, 10, 8, 4);

        glMaterialfv(GL_FRONT, GL_AMBIENT,    leavesMaterial.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    leavesMaterial.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   leavesMaterial.specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   leavesMaterial.shininess);

        glTranslatef(0, 0, 10);
        gluSphere(quadric, 5, 8, 8);
    glPopMatrix();
}

void viewport::bush()
{
    glPushMatrix();
        GLUquadric *quadric = gluNewQuadric();
        glRotatef(90,0,1,0);

        glMaterialfv(GL_FRONT, GL_AMBIENT,    leavesMaterial.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    leavesMaterial.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   leavesMaterial.specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   leavesMaterial.shininess);

        glTranslatef(2,0,0);
        gluSphere(quadric, 5, 8, 8);
        glTranslatef(-4,0,0);
        gluSphere(quadric, 5, 8, 8);
    glPopMatrix();
}

void viewport::bench()
{
    glPushMatrix();
        this->cuboid(&woodMaterial, 12, 4, 4);
        glTranslatef(0, 0, 2);
        this->cuboid(&woodMaterial, 12, 8, 1);
    glPopMatrix();
}

void viewport::car()
{
    glPushMatrix();
        glTranslatef(0,4,0);
        glPushMatrix();
            // Here are the normals, correctly calculated for the cube faces below
            GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0., 1., 0.}, {0., -1., 0.} };

            // Top face
            // This is done first so that the cuboid material doesn't interfere with the texture.
            glEnable(GL_TEXTURE_2D);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glEnable(GL_TEXTURE1);

            glBindTexture(GL_TEXTURE_2D, markusTexture);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, markusSwap.width(), markusSwap.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, markusSwap.bits());

            glNormal3fv(normals[4]);
            glBegin(GL_POLYGON);
                glTexCoord2f(1.0f, 1.0f);
                glVertex3f( 4,  3,  4);
                glTexCoord2f(1.0f, 0.0f);
                glVertex3f( 4,  3, -4);
                glTexCoord2f(0.0f, 0.0f);
                glVertex3f(-4,  3, -4);
                glTexCoord2f(0.0f, 1.0f);
                glVertex3f(-4,  3,  4);
            glEnd();

            glDisable(GL_TEXTURE1);
            glDisable(GL_TEXTURE_2D);

            glMaterialfv(GL_FRONT, GL_AMBIENT,    carMaterial.ambient);
            glMaterialfv(GL_FRONT, GL_DIFFUSE,    carMaterial.diffuse);
            glMaterialfv(GL_FRONT, GL_SPECULAR,   carMaterial.specular);
            glMaterialf(GL_FRONT, GL_SHININESS,   carMaterial.shininess);

            // Front face
            glNormal3fv(normals[0]);
            glBegin(GL_POLYGON);
                glVertex3f( 4,  3,  4);
                glVertex3f( 4,  3, -4);
                glVertex3f( 4, -3, -4);
                glVertex3f( 4, -3,  4);
            glEnd();

            // Back face
            glNormal3fv(normals[1]);
            glBegin(GL_POLYGON);
                glVertex3f(-4,  3,  4);
                glVertex3f(-4,  3, -4);
                glVertex3f(-4, -3, -4);
                glVertex3f(-4, -3,  4);
            glEnd();

            // Right face
            glNormal3fv(normals[2]);
            glBegin(GL_POLYGON);
                glVertex3f( 4,  3,  4);
                glVertex3f( 4, -3,  4);
                glVertex3f(-4, -3,  4);
                glVertex3f(-4,  3,  4);
            glEnd();

            // Left face
            glNormal3fv(normals[3]);
            glBegin(GL_POLYGON);
                glVertex3f( 4,  3, -4);
                glVertex3f( 4, -3, -4);
                glVertex3f(-4, -3, -4);
                glVertex3f(-4,  3, -4);
            glEnd();

            // Bottom face
            glNormal3fv(normals[5]);
            glBegin(GL_POLYGON);
                glVertex3f( 4, -3,  4);
                glVertex3f( 4, -3, -4);
                glVertex3f(-4, -3, -4);
                glVertex3f(-4, -3,  4);
            glEnd();

            glTranslatef(6,-1,0);
            this->cuboid(&carMaterial, 4, 4, 8);
            glTranslatef(-12,0,0);
            this->cuboid(&carMaterial, 4, 4, 8);
        glPopMatrix();

        glPushMatrix();
            GLUquadric *quadric = gluNewQuadric();
            glMaterialfv(GL_FRONT, GL_AMBIENT,    roadMaterial.ambient);
            glMaterialfv(GL_FRONT, GL_DIFFUSE,    roadMaterial.diffuse);
            glMaterialfv(GL_FRONT, GL_SPECULAR,   roadMaterial.specular);
            glMaterialf(GL_FRONT, GL_SHININESS,   roadMaterial.shininess);

            glTranslatef(6,-3,3);
            gluCylinder(quadric, 2, 2, 2, 16, 4);
            glTranslatef(0,0,2);
            gluDisk(quadric, 0, 2, 16, 4);
            glTranslatef(0,0,-10);
            gluCylinder(quadric, 2, 2, 2, 16, 4);
            gluDisk(quadric, 0, 2, 16, 4);
            glTranslatef(-12,0,0);
            gluDisk(quadric, 0, 2, 16, 4);
            gluCylinder(quadric, 2, 2, 2, 16, 4);
            glTranslatef(0,0,10);
            gluDisk(quadric, 0, 2, 16, 4);
            glTranslatef(0,0,-2);
            gluCylinder(quadric, 2, 2, 2, 16, 4);
        glPopMatrix();
    glPopMatrix();
}

void viewport::sign()
{
    glPushMatrix();
        glTranslatef(0, 14, 0);

        // Here are the normals, correctly calculated for the cube faces below
        GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0., 1., 0.}, {0., -1., 0.} };

        // Top face
        // This is done first so that the cuboid material doesn't interfere with the texture.
        glEnable(GL_TEXTURE_2D);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glEnable(GL_TEXTURE2);

        glBindTexture(GL_TEXTURE_2D, worldTexture);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, worldSwap.width(), worldSwap.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, worldSwap.bits());

        // Front face
        glNormal3fv(normals[0]);
        glBegin(GL_POLYGON);
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f( 4,  4,  4);
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 4,  4, -4);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 4, -4, -4);
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f( 4, -4,  4);
        glEnd();

        // Back face
        glNormal3fv(normals[1]);
        glBegin(GL_POLYGON);
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-4,  4,  4);
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f(-4,  4, -4);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f(-4, -4, -4);
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-4, -4,  4);
        glEnd();

        // Right face
        glNormal3fv(normals[2]);
        glBegin(GL_POLYGON);
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 4,  4,  4);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 4, -4,  4);
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-4, -4,  4);
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-4,  4,  4);
        glEnd();

        // Left face
        glNormal3fv(normals[3]);
        glBegin(GL_POLYGON);
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 4,  4, -4);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 4, -4, -4);
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-4, -4, -4);
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-4,  4, -4);
        glEnd();

        // Top face
        glNormal3fv(normals[4]);
        glBegin(GL_POLYGON);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 4,  4,  4);
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 4,  4, -4);
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-4,  4, -4);
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-4,  4,  4);
        glEnd();

        // Bottom face
        glNormal3fv(normals[5]);
        glBegin(GL_POLYGON);
            glTexCoord2f(1.0f, 1.0f);
            glVertex3f( 4, -4,  4);
            glTexCoord2f(1.0f, 0.0f);
            glVertex3f( 4, -4, -4);
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(-4, -4, -4);
            glTexCoord2f(0.0f, 1.0f);
            glVertex3f(-4, -4,  4);
        glEnd();

        glDisable(GL_TEXTURE2);
        glDisable(GL_TEXTURE_2D);

        glTranslatef(0, -14, 0);

        glRotatef(-90,1,0,0);
        GLUquadric *quadric = gluNewQuadric();

        glMaterialfv(GL_FRONT, GL_AMBIENT,    signMaterial.ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    signMaterial.diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   signMaterial.specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   signMaterial.shininess);

        gluCylinder(quadric, 2, 2, 10, 8, 4);
    glPopMatrix();
}

void viewport::scene(int buildw, int buildh, int pave1w, int roadw, int pave2w, int doorr, int liftp, int carr)
{
    // The inputs are doubled at the beginning to prevent rounding errors when they are halved later.
    buildw *= 2;
    buildh *= 2;
    pave1w *= 2;
    roadw  *= 2;
    pave2w *= 2;

    // Generate skyscraper.
    glPushMatrix();
        glTranslatef(0,buildh/2,0);
        this->skyscraper(buildw, buildh, buildw);
    glPopMatrix();

    // Distance to the far edge of the inner pavement.
    int pave1d = buildw/2 + pave1w;

    // Generate inner pavement.
    glPushMatrix();
        glTranslatef(0,-1,0);
        this->cuboid(&pavementMaterial, 2*pave1d, 2, 2*pave1d);
    glPopMatrix();

    // Distances to the centre and far edge of the road, respectively.
    int roadm = pave1d + roadw/2;
    int roadd = pave1d + roadw;

    // Generate road.
    glPushMatrix();
        glTranslatef(0,-2,0);
        glPushMatrix();
            glTranslatef(roadm,0,0);
            this->cuboid(&roadMaterial, roadw, 2, roadm*2 + roadw);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-roadm,0,0);
            this->cuboid(&roadMaterial, roadw, 2, roadm*2 + roadw);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0,0,roadm);
            this->cuboid(&roadMaterial, 2*pave1d, 2, roadw);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0,0,-roadm);
            this->cuboid(&roadMaterial, 2*pave1d, 2, roadw);
        glPopMatrix();
    glPopMatrix();

    // Distance to the centre, the far edge, and the length of the outer pavement, respectively.
    int pave2m = roadd + pave2w/2;
    int pave2d = roadd + pave2w;
    int pave2l1 = pave2d*2;
    int pave2l2 = pave1d*2;

    // Generate outer pavement.
    glPushMatrix();
        glTranslatef(0,-1,0);
        glPushMatrix();
            glTranslatef(pave2m, 0, 0);
            this->cuboid(&pavementMaterial, pave2w, 2, pave2l1);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-pave2m, 0, 0);
            this->cuboid(&pavementMaterial, pave2w, 2, pave2l1);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0, 0, pave2m);
            this->cuboid(&pavementMaterial, pave2l2, 2, pave2w);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0, 0, -pave2m);
            this->cuboid(&pavementMaterial, pave2l2, 2, pave2w);
        glPopMatrix();
    glPopMatrix();

    // Generate decoration.

    // Short sidewalk.
    glPushMatrix();
        glTranslatef(pave1d-4, 0, roadd+4);
        this->tree();

        for (int i = 0; i <= 1; i++) {
            glTranslatef(-pave1d+4, 0, 0);
            this->tree();
        }
    glPopMatrix();

    glPushMatrix();
        glTranslatef(pave1d-4, 0, -roadd-4);
        this->tree();

        for (int i = 0; i <= 1; i++) {
            glTranslatef(-pave1d+4, 0, 0);
            this->tree();
        }
    glPopMatrix();

    // Long sidewalk.
    glPushMatrix();
        glTranslatef(roadd+4, 0, roadd+4);
        this->tree();
        glTranslatef(0, 0, -(roadd+4)/3);
        this->bush();
        glTranslatef(0, 0, -(roadd+4)/3);
        this->tree();
        glTranslatef(0, 0, -(roadd+4)/3);
        this->bush();
        glTranslatef(0, 0, -(roadd+4)/3);
        this->tree();
        glTranslatef(0, 0, -(roadd+4)/3);
        this->bush();
        glTranslatef(0, 0, -(roadd+4)/3);
        this->tree();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-roadd-4, 0, -roadd-4);
        this->tree();
        glTranslatef(0, 0, (roadd+4)/3);
        this->bush();
        glTranslatef(0, 0, (roadd+4)/3);
        this->tree();
        glTranslatef(0, 0, (roadd+4)/3);
        this->bush();
        glTranslatef(0, 0, (roadd+4)/3);
        this->tree();
        glTranslatef(0, 0, (roadd+4)/3);
        this->bush();
        glTranslatef(0, 0, (roadd+4)/3);
        this->tree();
    glPopMatrix();

    // Benches
    glPushMatrix();
        glTranslatef(pave1d-3,0,pave1d);
        glRotatef(-90,0,1,0);
        glTranslatef(-pave1d*2/3,0,0);
        this->bench();
        glTranslatef(-pave1d*2/3,0,0);
        this->bench();
        glTranslatef(0,0,(pave1d-3)*2);
        glRotatef(180,0,1,0);
        this->bench();
        glTranslatef(-pave1d*2/3,0,0);
        this->bench();
    glPopMatrix();

    // Distances to the centre and far edge of the outer pavement, respectively.
    int pave3m = pave2d + pave3w/2;
    int pave3l1 = pave2l1;
    int pave3l2 = pave2l2;

    // Generate outer buildings pavement.
    glPushMatrix();
        glTranslatef(0,-1,0);
        glPushMatrix();
            glTranslatef(pave3m, 0, 0);
            this->cuboid(&pavementMaterial, pave3w, 2, pave3l1);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-pave3m, 0, 0);
            this->cuboid(&pavementMaterial, pave3w, 2, pave3l1);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0, 0, pave3m);
            this->cuboid(&pavementMaterial, pave3l2, 2, pave3w);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0, 0, -pave3m);
            this->cuboid(&pavementMaterial, pave3l2, 2, pave3w);
        glPopMatrix();
    glPopMatrix();

    // Generate corner pavement.
    int cornerm = roadd + (pave2w + pave3w)/2;

    glPushMatrix();
        glTranslatef(0,-1,0);
        glPushMatrix();
            glTranslatef(cornerm, 0, pave3m);
            this->cuboid(&pavementMaterial, pave2w + pave3w, 2, pave3w);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(cornerm, 0, -pave3m);
            this->cuboid(&pavementMaterial, pave2w + pave3w, 2, pave3w);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-cornerm, 0, pave3m);
            this->cuboid(&pavementMaterial, pave2w + pave3w, 2, pave3w);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-cornerm, 0, -pave3m);
            this->cuboid(&pavementMaterial, pave2w + pave3w, 2, pave3w);
        glPopMatrix();
    glPopMatrix();

    // Generate exit roads.
    int road2l = pave2w + pave3w;

    glPushMatrix();
        glTranslatef(0,-2,0);
        glPushMatrix();
            glTranslatef(roadm, 0, roadd + road2l/2);
            this->cuboid(&roadMaterial, roadw, 2, road2l);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(roadm, 0, -roadd - road2l/2);
            this->cuboid(&roadMaterial, roadw, 2, road2l);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-roadm, 0, roadd + road2l/2);
            this->cuboid(&roadMaterial, roadw, 2, road2l);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-roadm, 0, -roadd - road2l/2);
            this->cuboid(&roadMaterial, roadw, 2, road2l);
        glPopMatrix();
    glPopMatrix();

    // Revolving door.
    int sidedoorl = pave1w/2;
    int doorl = buildw/2;

    if (doorl > sidedoorl)
        sidedoorl = doorl;

    glPushMatrix();
        glTranslatef(0, 0, buildw/2 + pave1w/4);
        glTranslatef(buildw/4,0,0);
        this->cuboid(&whiteShinyMaterial, 1, 16, sidedoorl);
        glTranslatef(-buildw/2,0,0);
        this->cuboid(&whiteShinyMaterial, 1, 16, sidedoorl);
        glTranslatef(buildw/4,0,0);

        glRotatef(doorr,0,1,0);
        this->cuboid(&whiteShinyMaterial, 1, 10, doorl);
        glRotatef(90,0,1,0);
        this->cuboid(&whiteShinyMaterial, 1, 10, doorl);
    glPopMatrix();

    // Lift.
    glPushMatrix();
        glTranslatef(0, liftp, -buildw/2);
        this->cuboid(&whiteShinyMaterial, buildw/4, buildh/4, buildw/4);
    glPopMatrix();

    // Car.
    glPushMatrix();
        glRotatef(-carr,0,1,0);
        glTranslatef(0, 0, roadd - roadw/4);
        this->car();
    glPopMatrix();

    // Signs.
    glPushMatrix();
        glTranslatef(buildw/2, 0, pave1d-pave1w/2);
        this->sign();
        glTranslatef(-buildw, 0, 0);
        this->sign();
    glPopMatrix();
}

// Called every time the widget gets changed.
void viewport::paintGL()
    {
    // Clear the widget.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Initialisation functions.
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glPushMatrix();
        glPushMatrix();
            glLoadIdentity();
            GLfloat light0_pos[] = { 150., 50.,  150., 1.};
            glLightfv(GL_LIGHT0, GL_POSITION, light0_pos);
            glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 75.);

            GLfloat light1_pos[] = {-150., 50.,  150., 1.};
            glLightfv(GL_LIGHT1, GL_POSITION, light1_pos);
            glLightf (GL_LIGHT1, GL_SPOT_CUTOFF, 75.);

            GLfloat light2_pos[] = { 150., 50., -150., 1.};
            glLightfv(GL_LIGHT2, GL_POSITION, light2_pos);
            glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, 75.);

            GLfloat light3_pos[] = {-150., 50., -150., 1.};
            glLightfv(GL_LIGHT3, GL_POSITION, light3_pos);
            glLightf (GL_LIGHT3, GL_SPOT_CUTOFF, 75.);

            GLfloat light4_pos[] = {0., 75., 0., 1.};
            glLightfv(GL_LIGHT4, GL_POSITION, light4_pos);
            glLightf (GL_LIGHT4, GL_SPOT_CUTOFF, 75.);

            glEnable(GL_LIGHT0);
            glEnable(GL_LIGHT1);
            glEnable(GL_LIGHT2);
            glEnable(GL_LIGHT3);
            glEnable(GL_LIGHT4);
        glPopMatrix();

        // World rotations.
        glRotatef(yangle,0,1,0);
        glRotatef(xangle,1,0,0);
        glRotatef(zangle,0,0,1);

        this->scene(buildw, buildh, pave1w, roadw, pave2w, doorr, liftp, carr);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // This defines the eye: where it is, what it's looking at, where the top of the view is.
    gluLookAt(10.,10.,10., 0.0,0.0,0.0, 0.0,1.0,0.0);

    // flush to screen
    glFlush();

    } // paintGL()
