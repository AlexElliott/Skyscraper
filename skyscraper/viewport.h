#ifndef GL_VIEWPORT_H
#define GL_VIEWPORT_H

#include <QGLWidget>
#include <QTimer>
#include <math.h>

class viewport: public QGLWidget
{
    Q_OBJECT

    QTimer* doortimer = new QTimer(this);
    QTimer* lifttimer = new QTimer(this);
    QTimer* cartimer = new QTimer(this);

    int yangle = 0;
    int xangle = 0;
    int zangle = 0;

    int buildw = 16;
    int buildh = 32;
    int pave1w = 10;
    int roadw  = 15;
    int pave2w = 10;
    int pave3w = 10;

    int doordirection = 1;
    int doorr = 0;
    int doorspeed = 10;
    float liftp = 0;
    int liftcount = 0;
    int liftspeed = 15;
    int cardirection = 1;
    int carr = 0;
    int carspeed = 10;

    int doorspeedstore;
    int liftspeedstore;
    int carspeedstore;

public:
    viewport(QWidget *parent);

protected:
    // called when OpenGL context is set up
    void initializeGL();
    // called every time the widget is resized
    void resizeGL(int w, int h);
    // called every time the widget needs painting
    void paintGL();

private:
    void polygon(int, int, int, int);
    void cuboid(struct materialStruct*, int width, int height, int depth);
    void skyscraper(int width, int height, int depth);
    void tree();
    void bush();
    void bench();
    void car();
    void sign();
    void scene(int buildw, int buildh, int pave1w, int roadw, int pave2w, int doorr, int liftp, int carr);

public slots:
    void yupdate(int newyAngle) {
        yangle = newyAngle;
        QGLWidget::update();
    }

    void xupdate(int newxAngle) {
        xangle = newxAngle;
        QGLWidget::update();
    }

    void zupdate(int newzAngle) {
        zangle = newzAngle;
        QGLWidget::update();
    }

    void buildwupdate(int newWidth) {
        buildw = newWidth;
        QGLWidget::update();
    }

    void buildhupdate(int newHeight) {
        buildh = newHeight;
        QGLWidget::update();
    }

    void pave1wupdate(int newWidth) {
        pave1w = newWidth;
        QGLWidget::update();
    }

    void roadwupdate(int newWidth) {
        roadw = newWidth;
        QGLWidget::update();
    }

    void pave2wupdate(int newWidth) {
        pave2w = newWidth;
        QGLWidget::update();
    }

    void doorupdate() {
        doorr += doordirection;
        doorr %= 360;
        QGLWidget::update();
    }

    void doorspeedupdate(int newdoorspeed) {
        if (doorspeed != 99999999) {
            doorspeed = 25 - newdoorspeed;
            doortimer->start(doorspeed);
        }
        else
            doorspeedstore = 25 - newdoorspeed;

        QGLWidget::update();
    }

    void liftupdate() {
        liftcount += 1;
        liftcount %= 360;
        liftp = buildh + ((buildh-buildh/4) * sin(liftcount * 3.14159 / 180));
        QGLWidget::update();
    }

    void liftspeedupdate(int newliftspeed) {
        if (liftspeed != 99999999) {
            liftspeed = 25 - newliftspeed;
            lifttimer->start(liftspeed);
        }
        else
            liftspeedstore = 25 - newliftspeed;

        QGLWidget::update();
    }

    void carupdate() {
        carr += cardirection;
        carr %= 360;
        QGLWidget::update();
    }

    void carspeedupdate(int newcarspeed) {
        if (carspeed != 99999999) {
            carspeed = 25 - newcarspeed;
            cartimer->start(carspeed);
        }
        else
            carspeedstore = 25 - newcarspeed;

        QGLWidget::update();
    }

    void doortoggleupdate() {
        if (doorspeed != 99999999) {
            doorspeedstore = doorspeed;
            doorspeed = 99999999;
        }
        else
            doorspeed = doorspeedstore;

        doortimer->start(doorspeed);
        QGLWidget::update();
    }

    void doorreverseupdate() {
        doordirection *= -1;
        QGLWidget::update();
    }

    void lifttoggleupdate() {
        if (liftspeed != 99999999) {
            liftspeedstore = liftspeed;
            liftspeed = 99999999;
        }
        else
            liftspeed = liftspeedstore;

        lifttimer->start(liftspeed);
        QGLWidget::update();
    }

    void cartoggleupdate() {
        if (carspeed != 99999999) {
            carspeedstore = carspeed;
            carspeed = 99999999;
        }
        else
            carspeed = carspeedstore;

        cartimer->start(carspeed);
        QGLWidget::update();
    }

    void carreverseupdate() {
        cardirection *= -1;
        QGLWidget::update();
    }
};

#endif // GL_VIEWPORT_H
