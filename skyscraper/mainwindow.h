#ifndef GL_MAINWINDOW_H
#define GL_MAINWINDOW_H

#include <QMainWindow>
#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QPushButton>
#include <QBoxLayout>
#include <QObject>
#include "viewport.h"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent);
    ~MainWindow();

    // The window layouts used.
    QBoxLayout *windowLayout;
    QBoxLayout *viewportLayout;
    QBoxLayout *settingsLayout;

    // The main widget.
    viewport *viewWidget;

    // Sliders for rotating the viewing angle.
    QSlider *nVerticesySlider;
    QSlider *nVerticesxSlider;
    QSlider *nVerticeszSlider;

    // Sliders for adjusting the scene geometry.
    QSlider *buildwSlider;
    QSlider *buildhSlider;
    QSlider *pave1wSlider;
    QSlider *roadwSlider;
    QSlider *pave2wSlider;

    // Sliders for adjusting animation speeds.
    QSlider *doorSlider;
    QSlider *liftSlider;
    QSlider *carSlider;

    // Buttons for stopping, starting, and reversing animations.
    QPushButton *doorToggle;
    QPushButton *doorReverse;
    QPushButton *liftToggle;
    QPushButton *carToggle;
    QPushButton *carReverse;

    // Resets all the interface elements.
    void ResetInterface();
};
#endif // GL_MAINWINDOW_H
