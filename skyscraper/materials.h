#ifndef MATERIALS_H
#define MATERIALS_H

#include <QImage>
#include <GL/glu.h>

// Setting up material properties
typedef struct materialStruct {
    GLfloat ambient[3];
    GLfloat diffuse[3];
    GLfloat specular[3];
    GLfloat shininess;
} materialStruct;

static materialStruct whiteShinyMaterial = {
    {1.0, 1.0, 1.0},
    {1.0, 1.0, 1.0},
    {1.0, 1.0, 1.0},
    100.0
};

static materialStruct roadMaterial = {
    {0.1, 0.1, 0.1},
    {0.1, 0.1, 0.1},
    {0.1, 0.1, 0.1},
    10.0
};

static materialStruct buildingMaterial = {
    {0.5, 0.5, 0.5},
    {0.5, 0.5, 0.5},
    {0.5, 0.5, 0.5},
    20.0
};

static materialStruct pavementMaterial = {
    {0.6, 0.6, 0.6},
    {0.6, 0.6, 0.6},
    {0.6, 0.6, 0.6},
    10.0
};

static materialStruct woodMaterial = {
    {0.6, 0.3, 0.0},
    {0.6, 0.3, 0.0},
    {0.4, 0.4, 0.4},
    20.0
};

static materialStruct leavesMaterial = {
    {0.0, 0.7, 0.1},
    {0.0, 0.7, 0.1},
    {0.4, 0.4, 0.4},
    10.0
};

static materialStruct carMaterial = {
    {0.3, 0.3, 1.0},
    {0.3, 0.3, 1.0},
    {0.3, 0.3, 1.0},
    20.0
};

static materialStruct signMaterial = {
    {0.1, 0.1, 0.1},
    {0.1, 0.1, 0.1},
    {0.1, 0.1, 0.1},
    30.0
};

unsigned int marcTexture;
unsigned int markusTexture;
unsigned int worldTexture;
QImage* marcOriginal = new QImage("..\\textures\\Marc_Dekamps.ppm");
QImage* markusOriginal = new QImage("..\\textures\\markus.ppm");
QImage* worldOriginal = new QImage("..\\textures\\Mercator-projection.ppm");
QImage marcSwap = marcOriginal->rgbSwapped();
QImage markusSwap = markusOriginal->rgbSwapped();
QImage worldSwap = worldOriginal->rgbSwapped();

#endif // MATERIALS_H
